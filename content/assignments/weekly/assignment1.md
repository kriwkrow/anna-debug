+++
title = "Week 01:Introduction"
description = "Giving credit to the images used in the demo"
summary = "Welcome to our introduction week!"
date = "2022-01-10"
author = "Anna Li"
feature_image = "/images/introduction.png"
+++



# As a beginner in Fablab 
 <!--more-->This week we learned how to create a [GitLab](https://about.gitlab.com/) repository and use its [CI tools](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ci_cd_template.html) to publish a basic [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML/Reference) page.
 We also learned the [basics of command line](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview) and [Git](https://git-scm.com/) to manage files in our repository.

 ## The challenge with computer/webpage
 My background is in architecture, and I am a beginner in webpage programming development. So, this week I took a lot of time dealing with many basic things. I would say Google and different tutorials are good friends, and I am happy to know some peers whose major is new media. They help me a lot. I am trying to record what I did this week; in case I miss some afterwards.

### The first week's assignment:
1. Link to your repository, e.g. https://gitlab.com/your-username/digital-fabrication/
2. Link to the published website, e.g. https://your-username.gitlab.io/digital-fabrication/

The challenge for me is how to link to my repository using cmder(there are many errors happening for a beginner!!!you really need more time to practice)
 ### Creating a new project in GitLab

{{< postimage url = "../../images/creat new project.png" alt ="throwing moon blocks" imgw ="100%">}}
Make sure your "visibility level" is **public**!

1. After creating a new project folder, you will creat a new **".gitlab-ci.yml" file**, apply a "html" template. 
With the commit message **"Add GitLab CI congiguration file to publish our website**





