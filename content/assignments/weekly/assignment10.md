+++
title = "Week 10: Embedded Programming"
description = "Giving credit to the images used in the demo"
summary = "This week is mostly using your own computer and trying to program different boards using different tool chains. No additional introductions at the lab are planned."
date = "2022-03-22"
author = "Anna Li"
feature_image = "/images/W10-image.jpg"
+++

# This week's assignment
* Compare the performance and development workflows for other architectures. You should try to program two different microcontroller boards.
* Read the datasheet for your microcontroller and identify information that could be useful for your project.
* Use your programmer to program your board to do something.
* Describe the programming process with text and images.
* Document the process on your documentation page.
* Include source files of the code you wrote.
* Add a hero video of your board working.
* Submit a link to your assignment page here.


### This week I mainly plan to familiar embedded system and programming which is very new for me. So at least in the beginning of this week, I don’t plan to redo or mill a new PCB board this week, I want to borrow the board in fablad to test the programming part. I could spend a lot of time on what I don’t know and make it clear what am I doing now. 

##### Embedded system

* ### what is Embedded system

* Embedded system is new for me, so I do some basic research on it. the reference link: https://www.codrey.com/embedded-systems/embedded-systems-introduction/

* An embedded system is a computer system—a combination of a computer processor, computer memory, and input/output peripheral devices—that has a dedicated function within a larger mechanical or electronic system.____from Wiki

 {{< postimage url = "/images/W9-embedded.jpg" alt ="throwing moon blocks" imgw ="80%">}} 


An embedded system on a plug-in card with processor, memory, power supply, and external 
interfaces.It uses a Microcontroller/Microprocessor to perform a single job. It is a stand-alone device with or without operating system. Examples may be a washing machine, Music player, ATM, Vending machine, Data Logger, etc. Nowadays, Most of the devices run on the OS (Operating System).

 {{< postimage url = "/images/W10-embedded system.png" alt ="throwing moon blocks" imgw ="80%">}} 

   Embedded hardware | Embedded software
--------|------
  {{< postimage url = "/images/W10-embedded hardware.png" alt ="throwing moon blocks" imgw ="90%">}} | {{< postimage url = "/images/W10-embedded software.png" alt ="throwing moon blocks" imgw ="90%">}}


* ### Embedded system structure

   {{< postimage url = "/images/W10-embedded system structure.png" alt ="throwing moon blocks" imgw ="60%">}} 

* Embedded Hardware:
a central processing unit(CPU) that serves as the main system controller. A CPU can be presented by : a microprocessor and a microcontroller
other essential embedded hardware elements include: memory, I/O, computer buses, sensors, analog-to-digital converters(ADC), digital-to-analog converters(DAC), actuators, peripherals.


* Embedded software:
The softeware layer may contain various components depending on the device’s complexity and purpose. A complete embedded software package includes four constituents: firmware, an operating system, middleware and application software.

#### Then I also do some research about Embedded system programming, which I found it was too much for me to understand this week, so I gived up this deeper research. 




##### Reading Datasheet

* I want to see more about the chips during Electronics design week, like Attiny412. The chips datasheet we could find by this link: https://www.digikey.com/product-detail/en/microchip-technology/ATTINY412-SSFR/ATTINY412-SSFRCT-ND

There are 594 pages documents for Attiny412…

 {{< postimage url = "/images/W10-Attiny412.png" alt ="throwing moon blocks" imgw ="100%">}} 



##### Using EDBG to Program D11C Boards

* #### Knowing Programmer SWD D11C
The link is: https://gitlab.fabcloud.org/pub/programmers/programmer-swd-d11c

The main features are:
1. Minimal BOM
2. Foolproof connectors with a mark on GND pin
3. LED's

* Pinout
There are two SWD connectors, the one closest to the SAMD11C14 should only be used once to flash the bootloader. After that, the other connector can be connected with an IDC cable to the target, respecting the orientation of the GND mark.

 {{< postimage url = "/images/W10-d11c-updi-1.png" alt ="throwing moon blocks" imgw ="80%">}} 

* Flashing firmware
This board must be flashed with the free dap binary from Alex Taradov, which can be downloaded here.
For flashing, edbg can be used with the following command:
edbg -ebpv -t samd11 -f free_dap_d11c_mini.bin
After successful flashing, your board should show up as a USB device named "CMSIS-DAP".

 {{< postimage url = "/images/W10-d11c-1.png" alt ="throwing moon blocks" imgw ="60%">}} 

 * Using as a Programmer

Simply connect a target board with an 2x2 IDC connector, keeping the orientation consistent so that the ground marks are on the same wire.
The most common application is to upload the sam BA bootloader from the Fab SAM core. You can download the binary for the bootloader here.
There are two methods to flash the bootloader onto the target board:

edbg:

edbg -ebpv -t samd11 -f sam_ba_SAMD11C14A.bin

Arduino: select SAMD11C14A as the board in the Fab SAM core, then click on Burn Bootloader. The SWD programmer should be automatically detected.

After a successful flashing, the target board should appear as a CDC serial port, and can now be programmed through USB from the Arduino IDE as you would for a commercial Arduino board.


* #### Programmer UPDI D11C

The link is from here: https://npk-stn.ru/2019/07/19/simple_programming_attiny414_via_updi/?lang=en

 {{< postimage url = "/images/W9-updi.jpg" alt ="throwing moon blocks" imgw ="80%">}} 

* Pinout
The 2x3 IDC connector is meant to be reversible, i.e. it can be connected in both orientations on the target side, as long as the same connector is replicated there, with at least one pad of each type routed to the AVR chip.
The SWD pins near the SAMD11C14 should only be used once when flashing the bootloader.

 {{< postimage url = "/images/W10-d11c-updi-1.png" alt ="throwing moon blocks" imgw ="80%">}}


##### Programming test

* ### Programming Atiny412

{{< youtube rYKtlCed9aw >}}

* ### Programming ATsamD11C14A

{{< youtube nUoRjPhh8do >}}