+++
title = "Week 14：Input Devices"
description = "Giving credit to the images used in the demo"
summary = "This week you will learn how to use analog and digital sensors with a microcontroller."
date = "2022-04-20"
author = "Anna Li"
feature_image = "/images/W14-soldering.jpg"
+++

# This week's assignment
* Design a microcontroller board with a sensor of your choice. Make sure the board has a working FTDI connection.
* Program your board to read values from the sensor and send them to your computer.
* Include a hero shot and source files of your board in your documentation.
* Describe what you learned about the sensor that you are using on your documentation website.
* Submit a link to your assignment page here.


#### Reference link from Fab Academy: https://academy.cba.mit.edu/classes/input_devices/index.html




### I don't have much experience for sensor before, so my goal this week is to start a simple board reading values from the sensor and sending them to my computer. The first thing is to read the datesheet.


##### Reading datasheet!

* ### Magnetic field
### TLE493DA2B6HTSA1(Sensor Hall Effect 12C), datasheet: https://media.digikey.com/pdf/Data%20Sheets/Infineon%20PDFs/TLE493D-A2B6_V1.3_4-9-19.pdf


  {{< postimage url = "/images/W14-datasheet.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

Tle493D-A2B6
Low Power 3D Hall Sensor with I2C Interface

  {{< postimage url = "/images/W14-sensor.jpg" alt ="throwing moon blocks" imgw ="50%">}} 

#### Applications
The TLE493D-A2B6 is designed for all kinds of sensing applications, including the following:

* Gear stick position
* Control elements in the top column module and multi function steering wheel
* Multi function knobs
* Pedal/valve position sensing

#### Sensing

Measures the magnetic field in X,Y and Z direction. Each X-,Y- and Z-Hall probe is connected sequentially to a multipexer, which is then connected to an Analog to Digital Converter(ADC). Optional, the temperature(default=activated) can be determined as well after the three Hall channels.

  {{< postimage url = "/images/W14-pinout.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

   {{< postimage url = "/images/W14-circuit.jpg" alt ="throwing moon blocks" imgw ="100%">}}  


### HallEffect_Analog_A1324LLHLT-T(what we currently have in fablab) 
the link:https://www.digikey.com/en/products/detail/allegro-microsystems/A1324LLHLT-T/2639989; 

datasheet: file:///E:/2021-2022%20aalto%20university%20study%20and%20job/1-%E8%AF%BE%E7%A8%8B/9-digital%20fabrication/week14/A1324-5-6-Datasheet.pdf

{{< youtube 9BFdGtvo9JE >}}


#### A1324, A1325, and A1326_ Low-Noise Linear Hall-Effect Sensor ICs with Analog Output

New applications for linear output Hall-effect devices, such as displacement, angular position, and current measurement, require high accuracy in conjunction with small package size. **The Allegro A1324LLHLT-T linear Hall-effect sensor ICs**  are designed specifically to achieve both goals. This temperature-stable device is available in a miniature surface mount package (SOT23W) and an ultra-mini through-hole single in-line package.These ratiometric Hall effect sensor ICs provide a voltage output that is proportional to the applied magnetic field. They feature a quiescent voltage output of 50% of the supply voltage.


   {{< postimage url = "/images/W14-1324.jpg" alt ="throwing moon blocks" imgw ="100%">}}  

   https://www.digikey.com/en/products/detail/allegro-microsystems/A1324LLHLT-T/2639989

   {{< postimage url = "/images/W14-1324-pinout.jpg" alt ="throwing moon blocks" imgw ="100%">}}  


   {{< postimage url = "/images/W14-1324-diagram.jpg" alt ="throwing moon blocks" imgw ="100%">}}  



##### Drawing a circuit!

* #### Install KiCad power library with the updated power symbols this week, because I almost forgot how to do it and noted here again. https://kicad.github.io/symbols/power

1. Kicad -> Preferences
2. Preferences -> Manage symbol libraries
3. Adding/updating new libraries.

I have some problems for "pin not connected" and "pin not driven" issues this week, very annoying for these small errors which makes me draw the wires many times, but still doesn't work. One solution is that checking the **Grid settings**, normally **1.27mm** is convient. But mine is not about the grid issue. I think mine is that "POWER-FLAG" doesn't work and I used the old version, so I download the one which Kris fixed and update it in the fab library, then it works very well. Another issue is that when I connect the **VDD** to capacitor, **GND** to capacitor, it also shows **not connected**. I add **POW_5V** and **POW_GND** where power connectors are, so it worked in the end. That's one issue I took some time to fix for drawing the board.


* #### Making one board with the analog one-axis hall effect sensor


* PCB

 {{< postimage url = "/images/W14-PCB (1).png" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W14-PCB (2).png" alt ="throwing moon blocks" imgw ="100%">}} 

* Milling

 {{< postimage url = "/images/W14-milling.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

* Soldering

 {{< postimage url = "/images/W14-soldering.jpg" alt ="throwing moon blocks" imgw ="100%">}} 



##### Testing the PCB board!

* #### Uploading the code by UPDI

 {{< postimage url = "/images/W14-uploading the data.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

{{< youtube EHIoPL-27ys >}}

#### Qestions: I am curious about the detection of the range for this small sensor. I need to test at a very close range, but out of this range the sensor is not that sensitive, how do I know this range??



##### Weight sensor

#### This week I also want to test weight sensor which we have in fablab -Adafruit Industries LLC 4543, https://www.digikey.fi/fi/products/detail/adafruit-industries-llc/4543/13148777

#### How to read weight values?

I find some TouTube videos showing how to connect a four-wire load cell to the amplifier and read the weight value with an Arduino.https://learn.sparkfun.com/tutorials/load-cell-amplifier-hx711-breakout-hookup-guide/all

https://forum.arduino.cc/t/measuring-strain-values-with-strain-gauge/681202


{{< youtube sxzoAGf1kOo >}}

{{< youtube pZpzdu97JYw >}}



##### My Final Project



#### 1.  Weight sensor

The programming test reference is from Kris, he gave me the Arduino tutorial for weight sensor. I decided to follow the link and test the weight sensor. And this part I used Ardunio Uno to test weight sensor.

1. **How to programm the Weight sensor**, I followed the link:  (https://botland.store/content/161-The-use-of-a-strain-gauge-beam-with-Arduino)

When I started to test weight sensor, every time the reading number is **0** in Arduino serial monitor. I changed the load cell, but still doesn't work. I thought they are broken, then later Matti found the **VDD** PIN which needs to connect power, and the board we had in fablab (Load Cell Amplifier HX711) needs two pins to connect power- VDD/VCC.

  {{< postimage url = "/images/F4-H7.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

2. **Programming**

For the code, I used the **https://github.com/sparkfun/HX711-Load-Cell-Amplifier/blob/master/firmware/SparkFun_HX711_KnownZeroStartup/SparkFun_HX711_KnownZeroStartup.ino**, and I just changed the pin code depending on my board. But firstly, I need set calibration_factor which makes sure your weight sensor reads the right number. So I follow the tutorial to make a simple scale with HX711.The main issue is to make sure that amplifier could be bended on both sides, it means you need to give some space between surface of the scale and load cell. 

   Before  | After
--------|------
  {{< postimage url = "/images/F4-scale-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/F4-scale-2.jpg" alt ="throwing moon blocks" imgw ="100%">}}

  **The code**

```

#include "HX711.h"

#define calibration_factor 105000 //This value is obtained using the SparkFun_HX711_Calibration sketch

#define DOUT  3
#define CLK  2

HX711 scale(DOUT, CLK);

void setup() {
  Serial.begin(9600);
  Serial.println("HX711 scale demo");

  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0

  Serial.println("Readings:");
}

void loop() {
  Serial.print("Reading: ");
  Serial.print(scale.get_units(), 1); //scale.get_units() returns a float
  Serial.print(" kg"); //You can change this to kg but you'll need to refactor the calibration_factor
  Serial.println();
}

```

{{< youtube UGawGc2DjWU >}}


3. **Using Attiny1614 to test weight sensor.**


For My final project I used my own design board-Attiny1614 to test weight sensor, even though it burned many times for some reasons, the weight sensor always worked very well. The following code is for testing weight sensors and the pin code could be changed.

Weight sensor worked very well seperately, but I found that when it worked with other motors or sensors, there was a delay in reading the data, so when the power was turned on, it took a long time for weight sensor to read the data and started to work. I have no idea about this issue right now, I need more testing to figure out afterwards. 


  **The code**

```
  Arduino pin 2 -> HX711 CLK
 3 -> DAT
 5V -> VCC
 GND -> GND
 
 The HX711 board can be powered from 2.7V to 5V so the Arduino 5V power should be fine.
 
*/

#include "HX711.h"

#define calibration_factor 108500 //This value is obtained using the SparkFun_HX711_Calibration sketch

#define DOUT  1
#define CLK  0

HX711 scale(DOUT, CLK);

void setup() {
  Serial.begin(9600);
  Serial.println("HX711 scale demo");

  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare();	//Assuming there is no weight on the scale at start up, reset the scale to 0

  Serial.println("Readings:");
}

void loop() {
  Serial.print("Reading: ");
  Serial.print(scale.get_units(), 1); //scale.get_units() returns a float
  Serial.print(" kg"); //You can change this to kg but you'll need to refactor the calibration_factor
  Serial.println();
}

```


{{< youtube AMiNMX4UhOk >}}



  **Final project code**

  
```  
//Ardunio uno
//Final project
//Inclucing one stepper motor, one servo motor, two hall effect sensors, one button,one weight sensor
//ANNA AND CATS AND M



//For servo motor
#include <Servo.h>
#define SERVO_PIN1 6
#define SERVO_PIN2 9
Servo myservo1;  // create servo object to control  servo motor 1
Servo myservo2;  // create servo object to control  servo motor 2


// For the weight sensor
#include "HX711.h"

#define calibration_factor 108500 //This value is obtained using the SparkFun_HX711_Calibration sketch

#define DOUT  2
#define CLK  3

HX711 scale(DOUT, CLK);


// For stepper motor
#define DIR_PIN          5
#define STEP_PIN         4

#define DIRFORWARD LOW
#define DIRBACKWARD HIGH

//For halleffect sensor

#define SENSOR_PIN1 A1  //analog input pin to hook the sensor to, creates a variable integer called"sensorPin"
#define SENSORTARGETVALUE1 850

#define SENSOR_PIN2  A2
#define SENSORTARGETVALUE2 600

// For button

#define BUTTON_PIN 10



// This is for servo motor to move the scoop up ad down
void scoopUp() {
  myservo1.write(0);
  myservo2.write(180);
  delay(2000);

}

void scoopDown() {
  myservo1.write(90);
  myservo2.write(90);
  delay(2000);
}



// For detect if scoop close to the edge of the litter box
bool Halleffectsensorclose(int Pin, int TargetValue) {
  //if the sensor reads "close to the sensor" it means "true"; it reads "far from the sensor", it means "false"
  // define the vaule for the "distance"
  int value = analogRead(Pin);
  value = map(value, 0, 1024, 1024, 0);
  //Serial.print(" halleffectsensor ");
  //  Serial.print(Pin);
  //  Serial.print(" ");
  //
  //  Serial.print(TargetValue);
  //  Serial.print(" ");
  //
  //  Serial.println(value); // print value to Serial Monitor


  if (value > TargetValue) {
    return true;
  }
  else {
    return false;
  }

}


// detect if cat is there
bool CatIsOut() {
  //if the weightsensor read the value">2kg", it means cats are in, so the stepper motor stops, otherwise it moves
  float value = scale.get_units();
//    Serial.print(" weightsensor ");
//    Serial.print( value ); //scale.get_units() returns a float
//    Serial.print(" kg"); //You can change this to kg but you'll need to refactor the calibration_factor
//    Serial.println();

  if (value < 1.0) {
    return true;
  }
  else {

    return false;
  }
}

void motoronestep() {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(50);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(50);
}

void scoopMove(int sensorPin, int TargetValue) {
  //when the scopp is far from the halleffect sensor, the motor continue to move
  while ( !Halleffectsensorclose(sensorPin, TargetValue) && CatIsOut()) {
    //when the cat is out, the stepper motor moves
    motoronestep();
  }
}



// the scoop moves forward if there is no cat, including detect if it is close to the edge of the box
void scoopMoveForward() {
  digitalWrite(DIR_PIN, DIRFORWARD); // Look at the define
  scoopMove(SENSOR_PIN1, SENSORTARGETVALUE1);

}

void scoopMoveBackward() {
  digitalWrite(DIR_PIN, DIRBACKWARD); // Look at the define
  scoopMove(SENSOR_PIN2, SENSORTARGETVALUE2);

}


//function for testing stepper motor and scoop
void teststepper() {
  scoopDown();
  int steps = 4000;
  digitalWrite(DIR_PIN, DIRFORWARD);
  for (int i = 0; i < steps; i++) {
    motoronestep();
  }
  digitalWrite(DIR_PIN, DIRBACKWARD);
  for (int i = 0; i < steps; i++) {
    motoronestep();
  }
  scoopUp();
}




void setup() {
  Serial.begin(9600);
  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare();  //Assuming there is no weight on the scale at start up, reset the scale to 0
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  myservo1.attach(SERVO_PIN1);  // attaches the servo on pin  to the servo object
  myservo2.attach(SERVO_PIN2);  // attaches the servo on pin  to the servo object
}


void loop() {




  //int buttonSmartphone = HIGH; // button pin connects PA3
  //buttonSmartphone = digitalRead (BUTTON_PIN);

  //if (buttonSmartphone == HIGH) {
  if (true) {
    scoopMoveForward();
    scoopUp();
    scoopMoveBackward();
    scoopDown();
  }



  // put your main code here, to run repeatedly:

}

```

 

#### 2.  Halleffect sensor

For my final project, I used two hall-effect sensors to detect the magnet which helps me to control when the linear movement stops and avoid them touching the edge of the litter box. For this purpose, I would say the hall effect sensor worked well. But my TMC2208 board often took high current suddenly, but I could not figure out what the problem was, so most of the time I used manually to move the linear-motion bearing trails and tested the hall-effect sensor. You could easily read the values in the serial monitor, when the magnet was close to the sensor, the number increased. In the beginning, I could read the number difference apparently, so I added extra two magnets, and it worked.

You could see my PCB boards in the following image.

{{< postimage url = "/images/W14-halleffect sensor.jpeg" alt ="throwing moon blocks" imgw ="50%">}} 

{{< postimage url = "/images/W14-boards.jpg" alt ="throwing moon blocks" imgw ="50%">}} 


   Hall effect sensor  | Hall effect sensor
--------|------
  {{< postimage url = "/images/W14-hall effect sensor-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W14-hall effect sensor-2.jpg" alt ="throwing moon blocks" imgw ="60%">}}

 {{< postimage url = "/images/W14-magnet.jpg" alt ="throwing moon blocks" imgw ="50%">}}  


   **The code**


```
//Fab Academy 2022
//Hall effect
//Adrianino
//ATtiny1614 - ATtiny1624


int sensorPin = 9;    // analog input pin to hook the sensor to
int sensorValue = 9;  // variable to store the value coming from the sensor
 
void setup() {
  Serial.begin(115200); // initialize serial communications
}
 
void loop() {
  sensorValue = analogRead(sensorPin); // read the value from the sensor
  sensorValue = map(sensorValue, 0, 1024, 1024, 0);
  Serial.println(sensorValue); // print value to Serial Monitor
  //mySerial.println("x"); // print value "x" to Serial Monitor
  delay(1500); // short delay so we can actually see the numbers
}

```



{{< youtube lLRWeROCB8U >}}


{{< youtube w3ct1OOgU3Q >}}


{{< youtube EJfAAc45UZA >}}





