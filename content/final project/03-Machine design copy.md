+++
author = "Anna Li"
title = "04-Machine building_self-clean cat litter box"
date = "2022-04-25"
description = "Testing the machine_self-clean cat litter box"
tags = [
    "markdown",
    "text",
]
feature_image = "/images/W13-image.jpg"
+++

### For the last two weeks, our group work is to make a machine with linear axis movement, then we make a very simple prototyping self-clean cat litter box. My goal is to test if the machine really works, including the mechanics part, programming part, and interface. The link for the step is : https://gitlab.com/aaltofablab/machine-building-2022b

What I did is from this link: https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment13/

{{< youtube mWaOvfo9lvk >}}

### Design problems needed to solve:
* #### How to reduce the weight of the machine and how to make the scoop go a litter faster; Acrylic is easy to break, can it be stronger and smaller afterwards?
* #### How to design a collection cat poop
* #### How to avoid to hurt cats with more smart ways?


1. ### The connection part:
small research about the connection part:

 {{< postimage url = "/images/F4-products.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/F4-rotation-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

{{< postimage url = "/images/F4-rotation-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

I have no idea to how to connect this two parts, I call it litter box and collecting the  poop.

2. ### About the material of the scoop? 




3. ### About the programming and interface? 
Probably continue to think about this issue next few weeks, but I have some inspiration from others' documentary. https://fabacademy.org/2020/labs/agrilab/students/florent-lemaire/projects/final-project-steps/#testing-samd. 

He used the SAMD, he also made two sensor vision(one is online, the other is offline). 

{{< postimage url = "/images/F4-board.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


{{< postimage url = "/images/F4-global ideas.jpg" alt ="throwing moon blocks" imgw ="100%">}} 












