+++
title = "Week 16：Interface and Application Programming"
description = "Giving credit to the images used in the demo"
summary = "This week you will learn how to establish communication between two or more PCBs that you made."
date = "2022-05-10"
author = "Anna Li"
feature_image = "/images/W16-image.jpg"
+++

# This week's assignment
* Write an application that interfaces a user with an input and/or output device that you made.
* Include a hero video and source files of the application in your documentation.
* Describe what you learned about application and interface building on your documentation website.
* Submit a link to your assignment page here.




### For the Machine Building week, Samuli did the interface part in our group. For my final project, I want to use a tool which is easy to get firstly and improve afterwards. For cat litter robot, I plan to make two user interface, one is shown on the telephone, the other one is shown on the machine. So basically, I want to make the user interface with buttons and 2D graphics. So I think **TK** is suitable for me, and I learn from  **TkDocs**(https://tkdocs.com/tutorial/intro.html#usage).


##### Creating the user interface for cat litter robot

* ### Installing Tk on Windows, firstly install Python. Recommend install Python 3 . 

* ### After installing python, you could check in python if there is Tk. 

   {{< postimage url = "/images/W16-tk test.jpg" alt ="throwing moon blocks" imgw ="50%">}}  

* ### Example practise

I learn this software through a example which needs a "calculate" button, the button will get the value out of that entry perform the calculation, and put the result in a label below the entry, also help me to be familiar with the layout. 

   {{< postimage url = "/images/W16-tk test-1.jpg" alt ="throwing moon blocks" imgw ="50%">}} 

### 1. Code

```
from tkinter import *
from tkinter import ttk

def calculate(*args):
    try:
        value = float(feet.get())
        meters.set(int(0.3048 * value * 10000.0 + 0.5)/10000.0)
    except ValueError:
        pass

root = Tk()
root.title("Feet to Meters")

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

feet = StringVar()
feet_entry = ttk.Entry(mainframe, width=7, textvariable=feet)
feet_entry.grid(column=2, row=1, sticky=(W, E))

meters = StringVar()
ttk.Label(mainframe, textvariable=meters).grid(column=2, row=2, sticky=(W, E))

ttk.Button(mainframe, text="Calculate", command=calculate).grid(column=3, row=3, sticky=W)

ttk.Label(mainframe, text="feet").grid(column=3, row=1, sticky=W)
ttk.Label(mainframe, text="is equivalent to").grid(column=1, row=2, sticky=E)
ttk.Label(mainframe, text="meters").grid(column=3, row=2, sticky=W)

for child in mainframe.winfo_children(): 
    child.grid_configure(padx=5, pady=5)

feet_entry.focus()
root.bind("<Return>", calculate)

root.mainloop()

```




### 2. The diagram notes.

I follow the documents step by step and read the functions at the same time, then make notes on the following image. I feel Tk is not difficult to learn at the beginning, but if you want to make the layout much better, you really need some time to test and try.

   {{< postimage url = "/images/W16-tk test-note.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


* ### Try to make my own interface with Tk. 

For my final project I just need some buttons, 2D graphics and maybe some simple text in the end. So I skipped a lot of more detailed learning for Tk and just wanted to test this software quickly. Also, I didn't finnish the 'communication of all my boards', I couldn't give a full test for the interface and communication, shall finnish it afterwards.


### 1. Code


```
from tkinter import *
from tkinter import ttk

import tkinter.font
from time import sleep

win=Tk()
win.title("Hello Cat")

myFont = tkinter.font.Font(family = "Helvetica", size=12, weight = "bold")

mainframe=ttk.Frame(win, padding ="20")
mainframe.grid(column =0,row=0,sticky=(N,W,E,S))
win.columnconfigure(0,weight=1)
win.rowconfigure(0,weight=1)



image = PhotoImage(file='cat2.png')
image_label = Label(mainframe, image=image)
image_label.grid(column=1,columnspan=2, row=4)

mode_label = Label(mainframe, text="For Catkeepers")
mode_label.grid(column=1, row=1,sticky=W)


button_a = Button(mainframe, text="GO")
button_a.grid(column=1,row=2,sticky=(W,E))


button_b = Button(mainframe, text="STOP")
button_b.grid(column=2,row=2,sticky=(W,E))

button_c = Button(mainframe, text="RESET")
button_c.grid(column=1,row=3,sticky=(W,E))

button_d = Button(mainframe, text="CAMERA")
button_d.grid(column=2,row=3,sticky=(W,E))

for child in mainframe.winfo_children():
    child.grid_configure(padx=5,pady=5)
    

win.mainloop()

```


### 2. Layout of my user interface with two interesting tests


   {{< postimage url = "/images/W16-interface.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


This is what I made with Tk, but it is horrible big, and I try to see if the MAC problems for IDLE running. I am not that sure, but I google this problems which shows the size sometimes is related to the **sticky**, it means the position you place it also affects the whole layout and size. I just skip this part and will try to another software. I got some inspirations from **Fablab UAE**(https://fabacademy.org/2021/labs/uae/assignments/week16/), they tested **MIT APP Inventor** . Because I want to make a user interface on the smartphone in the end, **MIT APP Inventor**(https://appinventor.mit.edu/) is a visual programming environment that allows everyone to build fully functional apps for Android and iOS smartphones and tablets. For my final project, I want to make communication with sensor, so I want to try this one. 

App Inventor is a **cloud-based tool**, which means you can create apps for phones or tablets right in your web browser. 

   {{< postimage url = "/images/W16-layout.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

   {{< postimage url = "/images/W16-layout-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

There are two interfaces: **designers** and **blocks**, so you could make layout on the designer interface and adding code in blocks interface. 


##### My Final Project

I decided to use MIT APP Inventor as my final project interface testing. But I haven’t finished this part for my final presentation. I will update later this summer for this part. 

















 













