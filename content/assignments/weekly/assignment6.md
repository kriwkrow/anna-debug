+++
title = "Week 06: Electronics Production"
description = "Giving credit to the images used in the demo"
summary = "This week we will explore the basics of electronics production. We will learn the basics of printed circuit board (PCB) production and create our own microcontroller programmer circuit from scratch. "
date = "2022-02-20"
author = "Anna Li"
feature_image = "/images/W6-image.jpg"
+++

## This week's assignment
* Characterize the design rules for the PCB production process at the Aalto Fablab.
* Make an in-circuit programmer. Create tool paths for the milling machine, mill it and stuff (solder components) the board.
* Test the board and debug if needed.
* Document the process in a new page on your documentation website using pictures and text.
* Submit the link of the page.

## My goal of this week is to familiarise myself with PCB milling and soldering. Knowing more about PCB(Printed Circuit Board) for the next electronic design week.


### PCB-Printed circuit board

* A printed circuit board (PCB) is a laminated sandwich structure of conductive and insulating layers. PCBs have two complementary functions. The first is to affix electronic components in designated locations on the outer layers by means of soldering. The second is to provide reliable electrical connections (and also reliable open circuits) between the component's terminals in a controlled manner often referred to as PCB design.A basic PCB consists of a flat sheet of insulating material and a layer of copper foil, laminated to the substrate.
                            from Wikipedia

PCBs can be single-sided (one copper layer), double-sided (two copper layers on both sides of one substrate layer), or multi-layer (outer and inner layers of copper, alternating with layers of substrate).

### Milling a PCB board

* Drill presses are used to cut through metal, wood, and other tough surfaces.
* Milling machines only cut through metal.
* Drilling cuts into a surface vertically, while milling does the same with the added bonus of cutting horizontally with the side of the bit. You can use either a drill press or a powered hand drill for drilling, but milling is only done with a milling machine.



### Programmer-UPDI-D11C

you could download the UPDI file by this link: https://gitlab.fabcloud.org/pub/programmers/programmer-updi-d11c
This week I just use this file to practice milling and soldering. (download the gerber file)

##### SRM-20_Introduction

1. Download the file on fabcloud.org
2. Open KiCad, open the circuit_the file programmer-UPDI PCB file.(each file means different layer)
3. open **Files** -> **dimensions** -> set **with a margin of 1 mm**
4. Notice that **the white cross** on the left bottom cover, it is usually should be (0,0). 
go to the **file** -> **origin** -> x:0;  y:0
5. Go to **Parameters** -> **tool library** to choose the tools which you use or you could double check if the tools are right. 
6. Choosing **PCB milling bits**, the bit which we choose depending on you know what you are doing here. 
firstly for milling, we usually use **0.2-0.5mm** to cut the top layer and traces, leave out the first one-**o.15mm** only if you know what you are doing.  **1mm** for cutting edge. Drills could only for vertical path, be careful for that!

**Tool library**
No.2: 0.2-0.5 engrave; No.3: 1mm Cutter.

 {{< postimage url = "/images/W6-milling-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

7. Go to **Parameters** -> **Selected tools** 
**engraving tools**: choosing 2 ; engraving speed: 10mm/s; **Cutting toll**: 3> 1mm cutter

8. Go to **Caculate contours**
you could set the parameters for the contours **4**, the less number the speed is quicker but it is harder for soldering.

9. For the text, you could choose **engrave tracks as centerlines** each time you edit some things, recaculate contours

10. Go the tool**Mill**, output: sections/1/2, one file for each tool; **XY-zero point, select white cross at this point**.  Z-zero point: circult surface. Then save the files. **Remember here different tool is to the different numbers**, you could double check in tool library. Because you should change the bits in between. 

11. Open **VPanel**

 {{< postimage url = "/images/W6-milling-4.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 12. Load the tool**0.2-0.5mm** on the machine. 
 13. Using the tape to fix the PCB board to the base.
 14. setting **Z origin**, giving enough space for this distance. 

 {{< postimage url = "/images/W6-milling-5.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 15. Make the tool hit the surface naturally. then click **Z** to set z origin point. then at this point you could lift the tool a little bit. 

{{< postimage url = "/images/W6-milling-6.jpg" alt ="throwing moon blocks" imgw ="100%">}}

16. Set **XY origin**. after setting the Z origin, you could move the tool to the place where you want to cut as the XY origin point. Then click the **XY button**. THEN START TO MILLING!

17. Select the milling file, hit **output**, and **RESUME** button, then start to work!

18. Changing the tool, **1mm** for cutting. Reset the **Z** again, but you don't need to set XY origin again. Then you could use the same way for cutting. 


##### MDX-40_Introduction

I used this machine to make my PCB this week, I feel it is quicker than SRM-20, and I didn't find any issues when I worked. At least I knew many peers were suffering with the SRM-20. 

 {{< postimage url = "/images/W6-MDX.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 * The switch. 

 {{< postimage url = "/images/W6-switch.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 * The difference between MDX-40 and SRM-20 is loading the tool(bit). 

 {{< postimage url = "/images/W6-the tools.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W6-milling-7.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 * The other difference is that **setting Z origin**. 


 {{< postimage url = "/images/W6-MDX-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 



 {{< postimage url = "/images/W6-MDX-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


##### Soldering part

1. Firstly, I should know the components which I need
* 2*3 connector: 1 ( CONN HEADER SMD 2*3 POS, P2.54MM)
* 2*2 connector: 1 ( CONN HEADER SMD 2*2 POS, P2.54MM)
* 4.99 register: 1 ( RESISTOR 4.99k 1206 SMD) 
* micro capacitor: 1 (CAPACITOR 1UF 50V 1206 SMD)

I feel at the beginning I have a hard time for a good soldering, I usually suddenly make two tracks to be connected or over soldered. Or the components are still floated on the board after soldering. So I make many practices here. 

 {{< postimage url = "/images/W6-soldering-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


 {{< postimage url = "/images/W6-soldering-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

* To modify the mistakes for soldering. 

  {{< postimage url = "/images/W6-soldering-3.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

* The final board. 

   {{< postimage url = "/images/W6-soldering-4.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

### Oh! I cut the extra edges of the copper in the end. And I ask help from Matti to test my board, then I found it doesn't work in the beginning. The reason is that two tracks which shouldn't be connected, but they are connected with soldering. I feel I need more practice for soldering afterwards. 