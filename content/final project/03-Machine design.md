+++
author = "Anna Li"
title = "03-Machine design research"
date = "2022-03-07"
description = "Some research on mechanism linear motions"
tags = [
    "markdown",
    "text",
]
feature_image = "/images/F3-sketch.jpg"
+++

## This week I mainly think about the mechanism linear motions and do some basic research.I also want to improve my design to propose a good moving loops.

##### how the Scoopfree self cleaning litter box work?

The production instruction for the scoopfree self cleaning litter box: https://intl.petsafe.net/media/manuals/PAL17-14282-en-us-user-guide-with-cycle-button-v9242012.pdf

I found many good videos on Youtube to show how the product works and evaluations for the product. The following video is just to show the working process, a little bit long but have many inspirations. I also find some diagrams for the loading the box.

* ### **What I found here that the machanism is simple for this product, but still has some problems for multi-cat families like me. The solid sometimes is stuck with the bottom of the tray, which might happen in my cat litter box usually. So proper power is good!**
* ### **The components'list what I need for the final project,These are physical things I need to make:**

 1. **Privacy Hood(opotional)**
 2.  **Litter box frame(plastic materials-maybe I need a vaccum former, and CNC machine for the mould)**
 3. **Scoop(rake-metal materials?might be easy to clean)**
 4.  **Proper sensor or IP camera(which is more expensive?lol)**
  5. **Waste trap cover? depending on how the collection box looks like**
 6.  **litter trap**
  7. **waste trap or waste collection box**

*  ### **What should I learn from the next few weeks for my final project？**
1. **Machine design( should I work in metalworkshop?)**
2. **Application learning( I want to make communications with machine and my telephone)**
3. **Embedded programming learning(I have no idea to make the motor works through some buttons or test sensor)**
4. **The materials I need to choose? A good and sustinable material for the final project**


  {{< postimage url = "/images/F3-scoop-1.png" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/F3-scoop-2.png" alt ="throwing moon blocks" imgw ="70%">}} 
 


{{< youtube Kn368vw1jso >}}

##### I need a good scoop movement!!

 {{< postimage url = "/images/F3-sketch-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

##### How I make the scoop work? Start from a linear movement next few weeks.


## 1. V-slots
Some references about the v-slot is here: https://store.fut-electronics.com/products/v-slot-20mm-x-60mm-linear-rail-1m-black-anodized

This vedio also introduces how the v-slot works.

{{< youtube v=W2GxLMvkjQw&t=673s >}}

## 2. Inspirations from fab academy

I found some references on the fabacademy to see how these machinism works, thanks for Solomon. shall to make a model in the machine week. 
This week I want to know more about the mechanisms.

### * Slider Crank

I probably to choose **slider crank** as one linear motion.

**more details through this link: https://fabacademy.org/2021/labs/taipei/students/suetyee-yeung/projects/final-project/**


 {{< postimage url = "/images/F3-inspriation.png" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/F3-inspriation-1.png" alt ="throwing moon blocks" imgw ="100%">}} 

### She used slider crank for crushing can.

 {{< postimage url = "/images/F3-inspriation-3.png" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/F3-slider crank mechanism.png" alt ="throwing moon blocks" imgw ="100%">}} 

 ### * linear slider

### some interesting webpages to know more about mechanics:
1. https://hackmd.io/@oscgonfer/Sy4XFlLME?type=view
2. https://www.cs.cmu.edu/~rapidproto/mechanisms/examples.html



Some references are from Fabacademy

 **1. This reference which I learnt that how to use fusion to make a machine model and familar with motion control. https://andrewsleigh.com/fab-slider/making-v1-building-the-machine/**

 {{< postimage url = "/images/F3-linear motion-1.png" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/F3-linear motion-2.png" alt ="throwing moon blocks" imgw ="100%">}} 

**2. This reference I learnt that how to make a whole machine component，Machine building: https://fabacademy.org/2021/labs/berytech/Machine_Building.html**


  {{< postimage url = "/images/F3-linear motion-3.png" alt ="throwing moon blocks" imgw ="100%">}} 


##### Making a good sckedule for my final project for next few weeks!



