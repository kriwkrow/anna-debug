+++
title = "Week 09: Computer-Controlled Machining"
description = "Giving credit to the images used in the demo"
summary = "This week we are going to learn how to design a bigger object using CAD and gain basic understanding how CNC milling works. Each of you is going to get a 120x120 cm of 18 mm thick sheet of plywood that you will use to CNC mill parts for your assignment."
date = "2022-03-14"
author = "Anna Li"
feature_image = "/images/W9-sketch image.jpg"
+++

# This week's assignment
* Participate one of the introduction sessions or watch instruction videos.
* Pay attention to what runout, alignment, feedrate and tool paths are.
* Design, CNC mill and assemble something big (meter scale). You will get a 1200x1200x18 mm sheet of plywood for your assignment. Make use of it.
* Document your process in a new page on your website.
* Add downloadable design files to your documentation page.
* Submit a link to the page on your documentation website.

### This week I want to make a coffee table in my apartment.Because the space is very limited for student apartment, and also I have two cats in the room. So the size and flexibility is very important. 

#### This week I took a lot of time to focus on the testing board, pocket cutting and also familar with CNC machine. Also I feel the quality of material is not that good, so probably I need to think about more easiler joints. This is also what I did in this week, but still there were lots of fault in this period, which makes me know about the machine.

# 1. 3D modeling

**1. There are sketches to test the details:** 

 {{< postimage url = "/images/W9-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W9-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

    {{< postimage url = "/images/W9-3.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

    {{< postimage url = "/images/W9-4.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

## **2. Models in Rhino with grasshopper:** 

*  **Models in Rhino:**

  {{< postimage url = "/images/W9-grass-1.png" alt ="throwing moon blocks" imgw ="100%">}} 


*  **Using Grasshopper to change the parameters:**

  {{< postimage url = "/images/W9-grass-2.png" alt ="throwing moon blocks" imgw ="100%">}}  


*  **The basic parametric design rules in the model:**

Because I don't know the thichness of the plywood. It is based on the board I pick which is not always the same each time, so I don't want to have this uncertain variable to modify my design each time. Because the holes on the main board is very related to the thickness of the plywood. My strategy is connect every parameters to a certain edge, so you could see the yellow edges are all related to the same element. So I could change them at the same time, and the width of the holes and inserting piece is all related to the same **number slider**，so the thing is easier to be controled.

 {{< postimage url = "/images/W9-grass-3.png" alt ="throwing moon blocks" imgw ="100%">}}

 {{< postimage url = "/images/W9-grass-4.png" alt ="throwing moon blocks" imgw ="100%">}}

# 2. Machine Testing

## 2.1 VCarve

**If you already to have a file which is drawn by other softwares, do remmember to save as the file format which could inport into VCarve. Because I think it is easier to draw tabs in VCare.**

 {{< postimage url = "/images/W9-11.PNG" alt ="throwing moon blocks" imgw ="100%">}}

  {{< postimage url = "/images/W9-12.PNG" alt ="throwing moon blocks" imgw ="100%">}}


  {{< postimage url = "/images/W9-13.PNG" alt ="throwing moon blocks" imgw ="100%">}}

 {{< postimage url = "/images/W9-14.PNG" alt ="throwing moon blocks" imgw ="100%">}}

 {{< postimage url = "/images/W9-15.PNG" alt ="throwing moon blocks" imgw ="100%">}}

  {{< postimage url = "/images/W9-16.PNG" alt ="throwing moon blocks" imgw ="100%">}}

  {{< postimage url = "/images/W9-17.PNG" alt ="throwing moon blocks" imgw ="100%">}}  

  {{< postimage url = "/images/W9-18.PNG" alt ="throwing moon blocks" imgw ="100%">}}  

 {{< postimage url = "/images/W9-19.PNG" alt ="throwing moon blocks" imgw ="100%">}}  

 {{< postimage url = "/images/W9-110.PNG" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W9-111.PNG" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W9-112.PNG" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W9-113.PNG" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W9-113.PNG" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W9-114.PNG" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W9-115.PNG" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W9-116.PNG" alt ="throwing moon blocks" imgw ="100%">}} 

  {{< postimage url = "/images/W9-117.PNG" alt ="throwing moon blocks" imgw ="100%">}}

## 2.2 CNC (Recontech 1312 CNC machine)

1. Turn on the machine.
  {{< postimage url = "/images/W9-machine-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}

2. Open Mach3, then tool moves to the original point of the  machine table.
Click these three buttons to set original.

  {{< postimage url = "/images/W9-machine-2.png" alt ="throwing moon blocks" imgw ="100%">}}

3. Install the tool.
After the tool moves to the original point, you could click on the computer keyboard, move the tool to a proper place for install the milling/drilling tools then.

  {{< postimage url = "/images/W9-machine-3.jpg" alt ="throwing moon blocks" imgw ="100%">}}

  {{< postimage url = "/images/W9-machine-4.jpg" alt ="throwing moon blocks" imgw ="100%">}}

 {{< postimage url = "/images/W9-machine-7.jpg" alt ="throwing moon blocks" imgw ="100%">}}

 {{< postimage url = "/images/W9-machine-5.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W9-machine-6.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W9-machine-8.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W9-machine-10.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 
4. Arrange the rubber bonds on the vacuum bed which could pull the materials downwards. I would say this part is super important, because I am strugging a lot for **PUMPING**. But sometimes the air goes through somewhere.

 {{< postimage url = "/images/W9-machine-9.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

**Good layout is very important!!**

 {{< postimage url = "/images/W9-final.jpg" alt ="throwing moon blocks" imgw ="100%">}} 
