+++
title = "Week 12: Output Devices"
description = "Giving credit to the images used in the demo"

date = "2022-04-30"
author = "Anna Li"
feature_image = "/images/W12-board.jpg"
+++

# This week's assignment
* Add an output device to a microcontroller board you've designed, and program it to do something.
* Measure the power consumption of an output device.
* Include a hero shot and source files of your board in your documentation.
* Submit a link to your assignment page here.



### This week my plan is to make a PCB board to drive DC motor and control a stepper motor with A4988 Driver and Arduino. But unfortunately, my board fails before testing the programming. So I tried to mill another board and hopefull if I have time I want to redesign my board agian, because I found this week's assignment would be better to be connected with week 15-Networking and communication.


##### * Start from the Kris's lecture-DC motor

### * PCB board

{{< postimage url = "/images/W12-pcb-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}

{{< postimage url = "/images/W12-pcb.jpg" alt ="throwing moon blocks" imgw ="100%">}}

   Front | Back
--------|------
  {{< postimage url = "/images/W12-front.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W12-back.jpg" alt ="throwing moon blocks" imgw ="100%">}}



{{< postimage url = "/images/W12-board.jpg" alt ="throwing moon blocks" imgw ="100%">}}

{{< postimage url = "/images/W12-attiny 4953.jpg" alt ="throwing moon blocks" imgw ="50%">}}

I made a big mistake this week, because I forgot to double check **PIN 1** for the chip of 4953. I don't notice the **little black hole** on the corner of the board, when I soldered this part I just put it on the board and my board never worked in the end, even though I checked the every wire on the board and still didn't find any problems. Matti helped me to check and told me that problem, the worse thing this time I also made wrong direction for ATtiny chip, I already milled three boards this week and then again.... Fortunately, Yoona and I had the same board for this week's assignment which we both followed Kris's lecture. So I decided to borrow Yoona's board to test firstly and fixed my board later or redesign my board......
I also found that it was smart not put every component in the same basket, for example, if the power breaks I probably would lose my core chip-attiny 412, which generated double the amount of work. So I will separetaed them on the two boards afterwards, also adding more pins to connecting TX/RX. So the following is what I did. 



### * Measuring the current

* If you already know the current limit of your device and you don't want to kill it, I would say it is better to do this and adjust to the current limit.

   Grabbing the test legs | Adjusting the current limit
--------|------
  {{< postimage url = "/images/W12-grabbing the test legs.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W12-adjust the current limit.jpg" alt ="throwing moon blocks" imgw ="100%">}}

  * Measure the current with multimeter


   Multimeter | Power supply
--------|------
  {{< postimage url = "/images/W12-multimeter.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W12-grabbing the test legs.jpg" alt ="throwing moon blocks" imgw ="100%">}}

  We could connect the multimeter and power supply to measure the current and see if there is difference comparing with power supply. 



{{< postimage url = "/images/W12-power supply-1.jpg" alt ="throwing moon blocks" imgw ="60%">}}


{{< postimage url = "/images/W12-power supply-3.jpg" alt ="throwing moon blocks" imgw ="60%">}}


{{< postimage url = "/images/W12-power supply-2.jpg" alt ="throwing moon blocks" imgw ="60%">}}


   Before | After
--------|------
  {{< postimage url = "/images/W12-dc motor.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W12-dc motor-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}

I didn't find the numbers on the screen of multimeter changing too much, actually, in this part the multimeter didn't give good result to compare the current, so I decide to change another one to see, or maybe the current could not be detected because it is not big enough. I skip this part and continue. The next step I want to drive the DC motor and turn on the led with power supply.


{{< postimage url = "/images/W12-drive the dc motor.jpg" alt ="throwing moon blocks" imgw ="60%">}}


{{< postimage url = "/images/W12-drive the led.jpg" alt ="throwing moon blocks" imgw ="60%">}}




### * Programming the PCB board

* Arduino code

```
const int Motor_mosfet = 4;

const int Motor_Pin1 = 1; // ATtiny pin 3
const int Motor_Pin2 = 2; // ATtiny pin 4

void setup()
{
  Serial.begin(9600);
  pinMode(Motor_mosfet, OUTPUT);
  pinMode(Motor_Pin1, OUTPUT); 
  pinMode(Motor_Pin2, OUTPUT); 
}

void loop()
{
  
  analogWrite(Motor_mosfet, 255);
  digitalWrite(Motor_Pin1, HIGH);
  digitalWrite(Motor_Pin2, LOW);
  delay(2000);

  
  analogWrite(Motor_mosfet, 100);
  digitalWrite(Motor_Pin1, LOW);
  digitalWrite(Motor_Pin2, HIGH);
  delay(2000);

  analogWrite(Motor_mosfet, 0);
  digitalWrite(Motor_Pin1, LOW);
  digitalWrite(Motor_Pin2, LOW);  
  Serial.println("motor on");
  delay(2000);
}

```


* Everything seems ready, but...

{{< postimage url = "/images/W12-uploading the code.jpg" alt ="throwing moon blocks" imgw ="100%">}}

But there are some errors happened here, that I never notice that the circuit of the UPDI with six pins, but I just used the one side with three pins, the other side pins are closed. so When I connect them, I found the UPDI uploading fails, so what I could do is to change the direction of cable to see if it works. But that is one thing I need to notice next time. 


{{< postimage url = "/images/W12-uploading the code.jpg" alt ="throwing moon blocks" imgw ="100%">}}  

{{< postimage url = "/images/W12-direction.jpg" alt ="throwing moon blocks" imgw ="100%">}}  



The uploading works very well!!!


{{< postimage url = "/images/W12-upload-1.jpg" alt ="throwing moon blocks" imgw ="100%">}}  

The followings are the video I made for the test.


* Power supply with LED

{{< youtube 5ZhQSB_75Eo >}}

* LED test

{{< youtube KHwk-ilP0aQ >}}

* Changing the direction 


{{< youtube DP3asor5_cg >}}

* DC motor- programming test

{{< youtube RfsOch5xYjA >}}


##### My Final Project

For my final project, I had suffered a lot for PCB boards testing, because most of the boards were broken in the end. So I was very careful about the electronic design and improved them, including adding more wires around the holes when soldering, I hoped there were no connection problems. So you may see many PCB boards, and I think the final version works well, at least at the beginning. But after testing for a long time, my attiny1614 didn’t work because of high current, it became very hot and you could feel the tiny smoke.

 For my final project, I need one stepper motor, two servo motors, one weight sensor and two hall effect sensors. All boards can be connected to 5V, except for the stepper motor driver board-tmc2208, which needs to be connected to 12V. The power board made me feel nervous, because sometimes I could feel the regulator very hot. But it is still smart to separate the power board and attiny1614, at least I could have a good board when the other is broken.

### Final version PCB board

* Super power boards. 

{{< postimage url = "/images/W12-super power board.png" alt ="throwing moon blocks" imgw ="100%">}}


* Attiny 1614.

{{< postimage url = "/images/W12-attiny1614.png" alt ="throwing moon blocks" imgw ="100%">}}

* TMC2208

{{< postimage url = "/images/W12-tmc2208.png" alt ="throwing moon blocks" imgw ="100%">}}

* The final boards.

{{< postimage url = "/images/W12-pcb board version2.jpg" alt ="throwing moon blocks" imgw ="50%">}}

{{< postimage url = "/images/W12-final boards.jpg" alt ="throwing moon blocks" imgw ="50%">}}

* I improved a little bit before DDL, because I found if I add power 5V and 12V in the same board, which increases the risk of my board burning out. In fact, my board did burn up before DDL. So the version 1 you saw here is the old version, the version 2 is the new ones. 

{{< postimage url = "/images/W12-final boards-1.jpg" alt ="throwing moon blocks" imgw ="50%">}}


* Connection between boards.

{{< postimage url = "/images/W12-boards.jpg" alt ="throwing moon blocks" imgw ="50%">}}

{{< postimage url = "/images/W12-boards-1.jpg" alt ="throwing moon blocks" imgw ="50%">}}

* Testing the connected voltage.

{{< postimage url = "/images/W12-voltage.jpg" alt ="throwing moon blocks" imgw ="50%">}}


* I placed the heat sink on the top surface of the regulator, but I felt it didn't work that well. When I test the code, the power board became hot slowly. I was so nervous that the board burnt.

{{< postimage url = "/images/W12-heat sink-1.jpg" alt ="throwing moon blocks" imgw ="50%">}}

{{< postimage url = "/images/W12-heat sink-2.jpg" alt ="throwing moon blocks" imgw ="50%">}}


* Set the Potentiometer for TMC2208  [TMC2208](https://wiki.fysetc.com/TMC2208/)

The link is to show how to set the stepper motor current: https://learn.watterott.com/silentstepstick/faq/#how-to-set-the-stepper-motor-current

{{< postimage url = "/images/W12-caculate current.png" alt ="throwing moon blocks" imgw ="50%">}}

{{< postimage url = "/images/W12-current.png" alt ="throwing moon blocks" imgw ="50%">}}

{{< postimage url = "/images/W12-current-1.jpg" alt ="throwing moon blocks" imgw ="50%">}}

{{< postimage url = "/images/W12-current-2.jpg" alt ="throwing moon blocks" imgw ="50%">}}


* Soldering more wires for connection.

{{< postimage url = "/images/W12-soldering more.jpg" alt ="throwing moon blocks" imgw ="50%">}}



* Old version PCB boards.

{{< postimage url = "/images/W12-boardversion-1.jpg" alt ="throwing moon blocks" imgw ="50%">}}



{{< postimage url = "/images/W12-boardversion-1-1.jpg" alt ="throwing moon blocks" imgw ="50%">}}


* The programming test for these boards, you could find in  [Week 15：Networking and Communication](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment15/)