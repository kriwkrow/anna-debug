+++
author = "Anna Li"
title = "02-Draft Sketch"
date = "2022-01-27"
description = "First sketches for the final project"
tags = [
    "markdown",
    "text",
]
feature_image = "/images/sketch 1.jpg"
+++

{{< postimage url = "/images/sketch2.jpg" alt ="throwing moon blocks" imgw ="100%">}}

The section of the cat litter box(drawn by Anna Li)

#### Inspiration from the reference

I found that last year there was a project for the **cat litter robot** (https://fabacademy.org/2020/labs/agrilab/students/florent-lemaire/projects/final-project/). He reused his original cat litter robot. When I read his documentary, which intrigued me. But he already has some parts from his previous cat litter robot. So I don't want to make everything complex, especially for the shape of the litter robot. Based on my research, cats usually need to be trained by this rotated ball. And my original shape is a open box.
The big challenge is to understand how it works for this machine.

{{< postimage url = "/images/F2-sketch3.jpg" alt ="throwing moon blocks" imgw ="100%">}}

I give this sketch to present how the scoop works and I hope to watch the cat shit to see if the cat is healthy, which is what I have been doing for the last six years.


