+++
title = "Week 08:Electronics Design"
description = "Giving credit to the images used in the demo"
summary = "This week we are going to learn how to design your own PCB using KiCad or another EDA software of your choice. We will also learn to use some of the electronics test equipment at the lab as well as additional PCB production tricks."
date = "2022-03-06"
author = "Anna Li"
feature_image = "/images/W8-image.jpg"
+++

## This week's assignment
* Use the test equipment at the Fablab to observe the operation of a microcontroller circuit board:
check operating voltage on the board with multimeter or voltmeter;
use oscilliscope to check noise of operating voltage and interpret a data signal.
* Redraw one of the echo hello-world boards or equivalent and
add (at least) a button and a LED (with current limiting resistor) or equvivalent input and output,
check the design rules, make it, test it.
* Document your process in a new page on your website.
* Submit a link to the page on your documentation website.


### This week's content was new to me and I feel I need much time to practice and get familiar with our courses.

#### This week I want to be familar with the test equipment at the Fablab....but I found I took a lot of time on electronic design or drawings.

## 1. How to use an Oscilloscope

* Watch this YouTube video to get an idea what an oscilloscope is. You are supposed to use it during this assignment week.


{{< youtube u4zyptPLlJI >}}

* Watch this YouTube video to get an idea what an oscilloscope is. One of the oscilloscopes we have is the same brand: Tektronix.


{{< youtube tzndcBJu-Ns >}}

## 2. How to use a multimeter

* Watch this YouTube video to get an idea what a multimeter is and how to use it to test continuity, voltage and the flow of current.

{{< youtube TdUK6RPdIrA >}}

## 3. Electronics Design tools

#### * SVG-PCB

You could learn from this link: **https://leomcelroy.com/svg-pcb-website/#/home**.

#### * KiCad

KiCad EDA
A Cross Platform and Open Source Electronics Design Automation Suite.
The followings are what I learnt from KiCad.

##### 1.Installation

You could download the software through this link: **https://www.kicad.org/**. 
Also you could find a guide for the Windows.
https://www.kicad.org/download/windows/

{{< postimage url = "/images/W8-kicad.png" alt ="throwing moon blocks" imgw ="100%">}}


#### For the starters in the fablab, shall sugest to use Fab Electronics Library for KiCad : 
https://gitlab.fabcloud.org/pub/libraries/electronics/kicad.

Because this library cover all the electronics components found in the official fafb inventory. Using this library should also make it easier to share KiCad project files between Mac, Windows and Linux systems.

 1. **https://gitlab.fabcloud.org/pub**  -> **libraries** -> **electronics** -> **Kicad**
2. **download the KiCad file**.

{{< postimage url = "/images/W8-file.png" alt ="throwing moon blocks" imgw ="100%">}}

3. **load the references of KiCad**

{{< postimage url = "/images/W8-import.png" alt ="throwing moon blocks" imgw ="100%">}}

{{< postimage url = "/images/W8-kicad-1.png" alt ="throwing moon blocks" imgw ="100%">}}

After everything is done, you could creat a new file afterwards.


##### 2.Hello-ATtiny412

### some learning research in advance from previous fablab documentary.The link is: https://fabacademy.org/2020/labs/oulu/students/noora-nyberg/assignments/week09/

### ATtiny 412 microcontroller dasta sheet

{{< postimage url = "/images/W8-kicad-16.png" alt ="throwing moon blocks" imgw ="100%">}}
{{< postimage url = "/images/w8-attiny.jpg" alt ="throwing moon blocks" imgw ="100%">}}



{{< postimage url = "/images/W8-kicad-2.png" alt ="throwing moon blocks" imgw ="100%">}}


1. **Creating a new file in KiCad called :Hello-ATtiny412-DF**
you may see on the left side, one is PCB file and the other is Schematic file. 

{{< postimage url = "/images/W8-kicad-2.png" alt ="throwing moon blocks" imgw ="100%">}}

2. **Double click the Schematic file**.

{{< postimage url = "/images/W8-kicad-3.png" alt ="throwing moon blocks" imgw ="100%">}}

This week's reference is: https://academy.cba.mit.edu/classes/embedded_programming/index.html#echo




3. **Redraw the echo hello-world board**

* Add a symbol -> Microcontroller_ATtiny412_SSFR

{{< postimage url = "/images/W8-kicad-5.png" alt ="throwing moon blocks" imgw ="100%">}}

* Add cap
{{< postimage url = "/images/W8-kicad-6.png" alt ="throwing moon blocks" imgw ="100%">}}

* Add updi
{{< postimage url = "/images/W8-kicad-7.png" alt ="throwing moon blocks" imgw ="100%">}}

* Add FTDI, when you hit **R** (a shortcut)on the keyboard you may rotate it.
{{< postimage url = "/images/W8-kicad-8.png" alt ="throwing moon blocks" imgw ="100%">}}

* Add a wire to make connections.
{{< postimage url = "/images/W8-kicad-9.png" alt ="throwing moon blocks" imgw ="100%">}}

The first thing I want to learn clearly**what is PCB Ground Plane**?(Because I am curious about which is ground, and google it in general)
so the link here probably is useful: https://resources.pcb.cadence.com/blog/2020-the-pcb-ground-plane-and-how-it-is-used-in-your-design#:~:text=The%20ground%20plane%20on%20a,may%20even%20occupy%20multiple%20layers.
* **The ground plane** on a printed circuit board is typically a large area of metal connected to the circuit ground. This area of metal is sometimes only a small portion of the board, or in a multi-layer design it may be one entire board layer. Depending on the needs of the design, it may even occupy multiple layers.

So here **GUN** means **ground**
{{< postimage url = "/images/W8-kicad-10.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Add a power port -> add a wire(if the wire is not connected with any component or you want to stop it , you could double click) -> add Power_GND -> add Power_+5v**

{{< postimage url = "/images/W8-kicad-12.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Hit the shortcut-"V" to get the value field.**

{{< postimage url = "/images/W8-kicad-11.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Add a global label, the label is "UPDI"**

* **Conn_PinHeader_UPDI_2*03_P2.54mm_Vertical_SMD** 
{{< postimage url = "/images/W8-kicad-13.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Conn_PinHeader_FTDI_1*06_P2.54MM_Horizontal_SMD**

add a no-connection flag(Q)

{{< postimage url = "/images/W8-kicad-14.png" alt ="throwing moon blocks" imgw ="100%">}}

**"TX" means transform, "RX" means receive.**
{{< postimage url = "/images/W8-kicad-15.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Adding a LED, adding a Resistor-“R"**.

Until now, the board is almost done. 

{{< postimage url = "/images/W8-kicad-18.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Fill in schematic symbol reference designators**
Default is fine.

{{< postimage url = "/images/W8-kicad-19.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Adding Power_PWR_FLAG, then run Electrical Rules Checker to check if there are errors or warnings**

{{< postimage url = "/images/W8-kicad-20.png" alt ="throwing moon blocks" imgw ="100%">}}

{{< postimage url = "/images/W8-kicad-21.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Open PCB in board editor**

{{< postimage url = "/images/W8-kicad-22.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Update PCB with changes made to schematic(F8)** 
Then you have all the components which all you need.

{{< postimage url = "/images/W8-kicad-24.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Layout the board**
* **Track -> Pre-defined track -> Net Class -> change the parameters of Default**
{{< postimage url = "/images/W8-kicad-25.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Click the Route tracks and connect the tracks**.
{{< postimage url = "/images/W8-kicad-26.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Go to the Edge.Cuts layer and draw a edge cutting line with the tool“draw a line**.

{{< postimage url = "/images/W8-kicad-27.png" alt ="throwing moon blocks" imgw ="100%">}}

* **"Show the design rules checker window" to see if there are errors or warnings**.

* **Hit Plot and generate gerber file**.

{{< postimage url = "/images/W8-kicad-28.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Generate Drill Files**.

{{< postimage url = "/images/W8-kicad-29.png" alt ="throwing moon blocks" imgw ="100%">}}

* **Add holes on the board. Add a foorprint -> hole.
(right click -> properties, or you may use shortcut "E"). change the Pad type: Through-hole**.

{{< postimage url = "/images/W8-kicad-30.png" alt ="throwing moon blocks" imgw ="100%">}}


* **I fogot to add a botton, so here I need to come back a little bit to add it. Back to the Schematic Editor then add a Button_B3SN.**

{{< postimage url = "/images/W8-kicad-31.png" alt ="throwing moon blocks" imgw ="100%">}}


{{< postimage url = "/images/W8-kicad-32.png" alt ="throwing moon blocks" imgw ="100%">}}

* **After I redraw the lines here, go back to "plot", plot and generate Drill files in the same "gerbers" folder**.

* **Export SVG File**.

#### After drawing the PCB board, for the checking part, I need to learn some basics about programme and Arduino basics.Then the following parts are from Matti's references. Thanks very much! very helpful to learn electronic design!


##### 3.Milling and Drilling, and Inzerting the rivets

### Needed to review introduction here for milling

1. open Copper CAM -> Programmer -> UPDI-DIIC-F-Cu.gbr
2. File -> Dimensions -> Margin (1mm)
3. File -> orgin -> Positon(0,0) to make sure the white cross is in the corner.
4. Parameters-> tool library to check the bits.
   milling bits: 1.00mm for cutter; 0.15mm for milling.
5. Parameters -> select tools
6. Set contours
7. if the width is not good enough, change width fo the track
8. 'opdi" -> text -> right click the mouse " engrave tracks as centerlises" 
9. Mill -> output
with cross (XY-zero point).

   PCB drawing | Cutting board
--------|------
  {{< postimage url = "/images/W8-board-2.jpg" alt ="throwing moon blocks" imgw ="70%">}} | {{< postimage url = "/images/W8-board.jpg" alt ="throwing moon blocks" imgw ="70%">}}

10. press tool for inzerting the rivet to connect the double side.
Because I have the holes, so I need to inzert the rivets to make connections. 

{{< postimage url = "/images/W8-press.jpg" alt ="throwing moon blocks" imgw ="100%">}}

{{< postimage url = "/images/W8-Favorit press tool-rivets.jpg" alt ="throwing moon blocks" imgw ="100%">}}


The rivets we could use with two sizes:

{{< postimage url = "/images/W8-rivets.jpg" alt ="throwing moon blocks" imgw ="100%">}}

{{< postimage url = "/images/W8-inzerting the rivet.jpg" alt ="throwing moon blocks" imgw ="100%">}}

Now it is almost done:
{{< postimage url = "/images/W8-board.jpg" alt ="throwing moon blocks" imgw ="100%">}}


##### 4.ARDUINO BASICS

I am very interesting this part, shall updated afterwards...

##### 5.Soldering again

### what components should I use? I make a tool library this week, shall updated on the same paper.

{{< postimage url = "/images/W8-tool.jpg" alt ="throwing moon blocks" imgw ="100%">}}


##### The problems I met...

My soldering is bad and my welding often floats on the board. So I think I need more practice.

{{< postimage url = "/images/w8-soldering.jpg" alt ="throwing moon blocks" imgw ="100%">}}

The soldering vedio reference is from :https://newmedia.dog/c/efa/tools/soldering/


{{< youtube IpkkfK937mU >}}


The testing reference is from this link：https://www.youtube.com/watch?v=Oi5mpWqeIDk&t=946s





