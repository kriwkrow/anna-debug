+++
title = "Week 03:Project Management"
description = "Giving credit to the images used in the demo"
summary = "This week you will learn more about what Fab Academy is and pick up additional skills that are going to help you with documentation later in the course."
date = "2022-01-24"
author = "Anna Li"
feature_image = "/images/W3-feature.png"
+++

## Struggling with Hugo theme this week:(

I would say it is not smart to modify a hugo theme directly for a beginner. This week I suffered a lot to undertand the author's logic, because I need give a right path to link with the right ‘url" file. And it is necessary to review the HTML basics during this learning period.

## How I make a new page "Project Management"

I used an existing hugo theme: https://github.com/jonathanjanssens/hugo-casper3-demo, which shows like the following image. 

{{< postimage url = "/images/W3-hugo theme.png" alt ="throwing moon blocks" imgw ="100%">}}
There are many post images on the homepage for this theme, which is what I want to show the inspirational images on my homepage for the whole project and different weekly assignment. So you may see clearly what I did in the end for the whole project. But this theme is very image heavy so I set a `feature_image`.
parameter in the front matter of any content. 

#### 1. Installation for the hugo theme

```
cd themes
git clone git@github.com:jonathanjanssens/hugo-casper3.git

```
Modify the baseURL in the `config.toml`:
baseURL = "https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/", which is used for publish to my gitlab.

#### 2. Before modifying the theme, I creat a new project folder on GitLab


Before I modify this theme, I create a new project called `Aalto Digital Fabrication-2022`. Because I want to use my previous project folder as a practice folder. I don't need to generate an new SSH key from the terminal here, because the key last time I created should be applied in this new project. 
Then next step I created a `.gitlab-ci.yml`file in my new project folder, the template is for `hugo`. My hugo version is `hugo_extended`, so I need to replace this .gitlab-ci.yml file with this:
```
 # This file is a template, and might need editing before it works on your project.
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Pages/Hugo.gitlab-ci.yml

---
# All available Hugo versions are listed here:
# https://gitlab.com/pages/hugo/container_registry
image: registry.gitlab.com/pages/hugo/hugo_extended:latest
# image: registry.gitlab.com/pages/hugo:latest



variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
    - hugo
  except:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

pages:
  script:
    - hugo
  artifacts:
    paths:
      - public
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

```
Next step is to clone the repository, thtat is to make a local copy of the remote repositry in my own machine. The clone address can be seen from the project page. Then I typed:
```
git clone ""

git status

git pull
```

Git status> you could check the status for your connectiong and commits, Git pull>is for downloading the file from your Gitlab. So I used this commond to give a new folder which is downloaded from my gitlab called `aalto-digital-fabrication-2022`. Then I could extract the hugo theme in this folder.

#### 3. Making a good structure for my template

I use the html editor called `Visual Studio code`, and I drag the folder `hugo-casper3` to my editor. You may see clear structure for this theme: content; layouts; resources; static( for the static, I put all the images in this folder which is a default path linking with different page); themes

* I creat a new **.md** file in the content, **content/assignments/weekly/assignment3**.

This theme offers a sample of basic Markdown syntax that can be used in Hugo content files, als it shows whether basic HTML elements are decorated with CSS in a Hugo theme. So I just read the markdown syntax that it provides and write my own content and give a good layout. I would say it is not difficult to follow these rules, but the most challenge for me is to give the right image/video link path when I modify it. I am not fully sure if it works until push to the gitlab. So this is really annoying thing I met!

* Giving text for completing my documentary
```
+++
title = "Week 03:Project Management"
description = "Giving credit to the images used in the demo"
summary = "This week you will learn more about what Fab Academy is and pick up additional skills that are going to help you with documentation later in the course."
date = "2022-01-24"
author = "Anna Li"
feature_image = "/images/W3-feature.png"
+++
```
* Posting my image 

For posting iamge, I give a `shortcode` and creat a .html file - postimage.html. Because I want to make every image or video which I post has a 100% size for the page layout.
```
<img class="post-img" src="{{.Get `url`}}" alt="{{.Get `alt`}}" width="{{.Get `imgw`}}" />
```
Then I add image with this:

```
{{ < postimage url = "/images/..." alt ="throwing moon blocks" imgw ="100%"> }}
```

But I met a big challenge for this part. Really thanks our teacher Matti to help me to solve this problem, because the theme uses the image I set in feature_image in many different places, so the path is not always the same. So when I push to the gitlab, there is no image showing up in my personal page.

I need to make a change for the path like this:
```
<img class="post-img" src="{{.Get `url` | relURL}}" alt="{{.Get `alt`}}" width="{{.Get `imgw`}}" />
```
I make a change here: | relURL

Then I google this meaning, it shows:
**abbr. (= RELURL; relative uniform resource locator) Relative URL**

**A form of URL that omits the domain and some or all of the directory names, leaving only the filename and extension (which may contain a partial list of directory names). The specified file is searched or located by its relative position to the pathname of the current file.**

What I understand here I need to relocate the specified file here. 

Untill now, I almost give the whole process for this webpage, then I need to upload my repository uing git which makes the webpage publish to the public.


#### 4. Uploading my repository using git

I use the following command to upload my changings here.

**Push an existing folder**

```
cd existing_folder
git init
git remote add origin git@gitlab.com:yeshouanna/aalto-digital-fabrication-2022.git
git add .
git commit -m "Initial commit" (for this commit, you could change what you want)
git push

```
But I also met some challenge just for connecting, sometimes it shows the error like this: 
{{< postimage url = "/images/W3-gitlab error.png" alt ="throwing moon blocks" imgw ="100%">}}

**So I test many times to see if it works. I think it is all what I did for this week's assignment, if you see all of this on this page in the end, it means I did it!**


