
+++
title = "Week 17：Wildcard Week"
description = "Giving credit to the images used in the demo"
summary = "This week you will explore a digital fabrication method beyond the capability of a basic fab lab. Please familiarize yourself with the available machines from the list below and use the [Choose your Machine](https://mycourses.aalto.fi/mod/choice/view.php?id=899315) resource to select a machine that you will be exploring."
date = "2022-06-13"
author = "Anna Li"
feature_image = "/images/W17-image.jpg"
+++

# This week's assignment

* Choose one of the proposed digital manufacturing technologies.
* Create a digital design to be manufactured with the technology/machine of your choice.
* Participate in one of the introduction sessions.
* Work together with the workshop master and document the process.
* Publish the documentation on your course documentation page.
* Submit a link to your documentation page here.

### The machines we could choose in Aalto
* Waterjet Cutter
* Zund Cutter
* Wire EDM Machine
* Vacuum Forming


For this week, I chose the [Waterjet-Cutter](https://www.omax.com/omax-waterjet/55100) machine to make the scoop, poop collector and litter box. It is friendly to use and could cut the mateirals quickly. Waterjet cutting technology consists of a very high-pressure waterjet ejected at three times the speed of sound from the cutting head. In case of applications on harder materials, the jet of water is mixed with an eco-friendly natural abrasive powder regarded as the actual ‘cutting tool’. Waterjet cutting systems stand out for their very high cutting precision and guarantee production with minimum waste. Waterjet solutions allow you to cut any shape, size or material up to a maximum thickness of 300 mm. By using the same tool and parameters during most operations, it is possible to easily cut different materials such as marble, granite, glass, ceramic, composite materials, metals, and alloys. We had an introduction course for Waterjet Cutter, and setting up the machine was straightforward. We got help from Kari, who is the master of the metal workshop, so we just sent the file to him directly. He also discussed with me about my final project, and got advice about the material I could use. Actually, I didn’t put in more effort on the machine itself compared with previous weeks. But I still learnt a lot from the master. 

  {{< postimage url = "/images/W17-waterjet.jpg" alt ="throwing moon blocks" imgw ="50%">}} 

  The image is from waterjetcorp.com (https://www.waterjetcorp.com/en/product-range/cutting/). 



##### Introduction course of Waterjet Cutter

This is an interesting experience to learn about the machine.Even though I am used to “fire things”, I would say waterjet is much safer compared with CNC and laser cutter. You could see the curtains around the machine, which prevents the water from splashing. This made me feel safer when testing this machine.

 {{< postimage url = "/images/W17-waterjet machine workshop.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W17-waterjet machine workshop-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 {{< postimage url = "/images/W17-waterjet machine workshop-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


One thing to pay attention to is that the file format for cutting should be **.dxf**, and you need to check the file to make sure there are no overlapped lines or no connections for each corner as the following image shows. 

 {{< postimage url = "/images/W17-waterjet file.png" alt ="throwing moon blocks" imgw ="50%">}} 

Setting up the machine, for example, setting the original XYZ.

 {{< postimage url = "/images/W17-setting the machine.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

Recoding the cutting process.

{{< youtube PZOpCvaoOuw >}}


##### Final peoject testing and final cutting

### * First test. 
1. The cutting file for the first test which tests the aluminum material and the shape of scoop and collector. I drawed the pattern with the grasshopper in Rhino to make the circle diameters different, testing the hole sizes for collecting cat litter. The biggest diameter is 10mm and smallest one is 4mm. After testing, I chose 6mm for my final project.

   Scoop | Collector
--------|------
  {{< postimage url = "/images/W17-scoop-1-Model.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/w17-scoop-net-1-Model.jpg" alt ="throwing moon blocks" imgw ="80%">}}

2. Sanding and bending the metal piece.


   Metal piece | Sanding
--------|------
  {{< postimage url = "/images/F4-metal.jpg" alt ="throwing moon blocks" imgw ="60%">}} | {{< postimage url = "/images/F4-sanding.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


   Machine for bending | Final shape
--------|------
  {{< postimage url = "/images/F4-bending.jpg" alt ="throwing moon blocks" imgw ="90%">}} | {{< postimage url = "/images/F4-metal-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 ### * Final cutting. 
 The material I chose is 1.5mm thick and very soft, which is good for bending but not good for welding. I will improved later for the design, but this time I just cut and simply bend them and then test if the machine(cat litter robot) is running good. The following is the file I sent to the workshop master Kari. I am not sure if the servo motor had enough support to balance the movement of the metal sheet, so I cut another square piece for a support .

 {{< postimage url = "/images/W17-waterjet cutting file.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

 You should bend the metal piece manually, and you may see there are many degrees of bending. I want to make a square box and I found it was hard to bend all the edges at the beginning and I asked Kris. You need to remove some pieces which give more space for other parts. But it also took me a long time to clean my fingers because of machine oil. 

 * The degrees on the side of the machine which you could bend the metal piece in some degree. 

  {{< postimage url = "/images/W17-bending machine-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


 * You need to leave more space for bending if your model has more than three edges. Even if you leave enough space, I think this machine still has some limitations for bigger model, and maybe paper tape is a good recommended helper.

   {{< postimage url = "/images/W17-bending machine.jpg" alt ="throwing moon blocks" imgw ="100%">}} 

* Be careful with your fingers, especially if there is some glue on your fingers. The glue will mix with machine oil existing on your fingers for a long time（at least one week with my experience).


  {{< postimage url = "/images/W17-bending machine-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


  {{< postimage url = "/images/W17-waterjet cutting metal.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


  {{< postimage url = "/images/W17-metal-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


  {{< postimage url = "/images/W17-metal-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


  {{< postimage url = "/images/W17-metal-3.jpg" alt ="throwing moon blocks" imgw ="100%">}} 



 ### * 3D model for my final project.

 I used Rhino to make a 3D model for my final project, which helps me know the mechanics more clearly. Even though it took a lot of time, it was really helpful in the end.For more details, you could see my final presentation webpage. I used a laser cutter to make most of them. 



   3D model |  3D model-1
--------|------
  {{< postimage url = "/images/W17-3D model.png" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W17-3D model-1.png" alt ="throwing moon blocks" imgw ="100%">}} 



  {{< postimage url = "/images/W17-pieces.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


  * It is very fast to use a laser cutter to make most of the pieces, but I also met some problems when I cut them. One is testing the speed, even though I test the speed with small pieces. When I cut the whole board（plywood or acrylic), some parts cannot be cut completely. It is because the base platform is not flat or the material is not flat at all. You could not avoid it especially when you find it is hard to find an extremely flat plywood sheet. My solution is to improve the cutting speed or tape the sheet to the machine platform. 



   Plywood(not fully cut) |  Acrylic(not fully cut)
--------|------
  {{< postimage url = "/images/W17-plywood.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W17-acrylic.jpg" alt ="throwing moon blocks" imgw ="100%">}} 





   Acrylic (cutting speed test) |  Acrylic (cutting speed test)
--------|------
  {{< postimage url = "/images/W17-speed test-1.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W17-speed test-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 



   Acrylic (cutting speed test) |  Acrylic (cutting speed test)
--------|------
  {{< postimage url = "/images/W17-acrylic testing.jpg" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W17-acrylic testing-2.jpg" alt ="throwing moon blocks" imgw ="100%">}} 



* The final image for the model.


  {{< postimage url = "/images/W17-final.jpg" alt ="throwing moon blocks" imgw ="100%">}} 
