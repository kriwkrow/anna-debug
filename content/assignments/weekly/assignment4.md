+++
title = "Week 04:Computer-Aided Design"
description = "Giving credit to the images used in the demo"
summary = "This week is about computer-aided design. I will familiarize myself with a variety of 2D vector and raster tools, but mainly focus on parametric 3D modelling tools."
date = "2022-02-09"
author = "Anna Li"
feature_image = "/images/W4-feature-1.jpg"
+++

The image is from https://www.behance.net/gallery/10821355/VECTOR-CAT

## This week's assignment
* Explore one or more 2D vector graphics tools.
* Explore one or more 2D raster graphics tools.
* Explore one or more 3D modeling tools.

I will show several tools I usually use to make 2D graphics and 3D models, like Photoshop, Illustrator, Rhino for parametric modeling, Sketch up, Blender for animation. Also, I will try to practice new tools which I never use this week, like Fusion 360, GIMP. I will evulate which tool is most suitable for my final project. 


## 2D graphics tools

### Adobe Illustrator

Adobe Illustrator is a vector graphics which I used most for our architectural diagrams. It is clear to see the pixels when zooming without losing image's quality.
You may create the new file with certain size, like A4,A3...The color mode is `CMYK`. My Illustrator is Chinese version. 
The file could be saved as kinds of format.


   Creating a new file | Exporting a image
--------|------
  {{< postimage url = "/images/W4-raster.png" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W4-raster-1.png" alt ="throwing moon blocks" imgw ="100%">}}
 

### Adobe Photoshop
Adobe Photoshop is a raster graphics editor which I use to edit image or make a beautiful scene rending, I usually add the sun light, building shadow or objects which I must do in rending software. It is also good, but sometimes it will lose quality when zooming in. But it is very convenient and easy to learn. The image is about what I did before and after by photoshop, which is easy to give a scene but sometimes it lose image qulity as a bitmap.

   Before | After
--------|------
  {{< postimage url = "/images/sketchup.png" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/sketchup.jpg" alt ="throwing moon blocks" imgw ="100%">}}


   View image size | Saving as...
--------|------
  {{< postimage url = "/images/W4-imagesize.png" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W4-imagesize-1.png" alt ="throwing moon blocks" imgw ="100%">}}

### GIMP

I Learn this open source software with this link: **https://www.youtube.com/watch?v=wLSvubMGb8A**
I never used it before, and just learnt step by step. GIMP is an open source software for free, and a 2D raster graphics editor.

* Firstly, step up GIMP with this link: https://www.gimp.org/downloads/thanks.html, you could choose your own language for this, mine is Chinese. 

* Secondly, explore this tool!

I think GIMP is similar to the Photoshop and easy to learn for people who already have Photoshop experience before. I think the most convient thing for GIMP is that it has "scale image" tool. And I found that the quality of image was still good when it was scaled. So it is a good tool to sacale the image before uploading to the webpage.

   Before | After
--------|------
  {{< postimage url = "/images/W4-scaleimage.png" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W4-scaleimage-1.png" alt ="throwing moon blocks" imgw ="100%">}}



## 3D modeling tools

### Rhino

I normally use Rhino with grasshopper to make a parametric model, because it is easy to visualise and change the parameters.

With this link for more details of Rhino with grasshopper: https://www.plethora-project.com/education/2017/5/31/rhino-grasshopper. I learnt from the webpage for basic grasshopper knowledge.

{{< postimage url = "/images/W4-rihno.png" alt ="throwing moon blocks" imgw ="100%">}}

It is easy to make connections with the Rhino and grasshopper. When you open the Rhino file and then use the commond `grasshopper` , then you have two operating sections, one for Rhino and the other for grasshopper.

   Before | After
--------|------
  {{< postimage url = "/images/W5-point.png" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W5-point-1.png" alt ="throwing moon blocks" imgw ="100%">}}

  The commond `slider`is related to the parameters for the model. I could change the number among the domain and edit the accuracy.

   Before | After
--------|------
  {{< postimage url = "/images/W4-parameter-1.png" alt ="throwing moon blocks" imgw ="100%">}} | {{< postimage url = "/images/W4-parameter-2.png" alt ="throwing moon blocks" imgw ="100%">}}

The following part is that I want to make a lamp for laser cutting.

{{< postimage url = "/images/W4-parametric design.png" alt ="throwing moon blocks" imgw ="100%">}}

1. Firstly, I need to give a `line`and `square`in Rhino interface, and using `seting one curve` to make a connection.
So they are our basic **guideline** and **frame**.(For this part, I usually use **curve** to set the line and square)

{{< postimage url = "/images/W4-curve.png" alt ="throwing moon blocks" imgw ="100%">}}

2. Constructing movement vector along guildline.Using the **slider** to control the points on the guildline which is divided by serval parts.You could also see the vector direction.

{{< postimage url = "/images/W4-dividing the line.png" alt ="throwing moon blocks" imgw ="100%">}}

3.Rotating frames( all at the startpoint of the guideline). I think the most difficult part for grasshopper is to deal with the **data**, especially data series. Now I want to copy these frames and make sure each frame has the same rotation rate and all at the startpoint of guideline.

{{< postimage url = "/images/W4-rotating.png" alt ="throwing moon blocks" imgw ="100%">}}

So firstly, I need subdivide total rotation into small rates for each fram, then I use `EVal`and `Series`. Then using `RotAxl`. 

4. Giving the thickness of each piece. Now frame is still frame, so I use `Boundary surfaces`to give a surface for each frame, and using `Extrude`to give a thickness of the box.

{{< postimage url = "/images/W4-lamp.png" alt ="throwing moon blocks" imgw ="100%">}}

4. The last step: don't forget to **Bake** for your model in the grasshopper, because grasshopper couldn't store any data which is saved in Rhino interface, in case next time you lose your **guildline** and **frame**, you need to bake your model in Rhino and save the **grasshopper file**. 

Now I have a parametric lamp!

I will update my practice for blender here. 





