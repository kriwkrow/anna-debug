
+++
title = "Week 18：Applications and Implications"
description = "Giving credit to the images used in the demo"
summary = "This week's assignment should be fairly easy, since all of you have been already working towards it for a long time."
date = "2022-06-14"
author = "Anna Li"
feature_image = "/images/W18-image.jpg"
+++

# This week's assignment

Propose a final project masterpiece that integrates the range of units covered.
Your project should incorporate:

* 2D and 3D design
* Additive and subtractive fabrication processes
* Electronics design and production
* Microcontroller interfacing and programming
* System integration and packaging.

Where possible, you should make rather than buy the parts of your project. Projects can be separate or joint, but need to show individual mastery of the skills, and be independently operable.

See  [Final Project Requirements](https://fabacademy.org/2022/nueval/applications_and_implications.html)for a complete list of requirements you must fulfil.

Have you answered these questions?


* What will it do?
* Who has done what beforehand?
* What will you design?
* What materials and components will be used?
* Where will they come from?
* How much will they cost?
* What parts and systems will be made?
* What processes will be used?
* What questions need to be answered?


# Cat litter robot_Meidao&Anteng

### * What will it do?

Cat litter robot is a self-cleaning litter box that provides a clean bed of litter for your cat. The robot could detect if the cat is by the weight sensor which makes the machine safer and avoid causing harm to the cats. You may click the button on your smartphone and control the machine remotely. The scoop could push the cat poop to the collector and store them in the collector, then you may just clean the collector each time. 

What’s more, the ambition for the cat litter robot is to help the owner know if their cat is healthy by pooping. It is not a joke because the frequency, the softness and color of the poop could indicate their health directly. So the next step which could improve this machine is to see how to transform these images into data which help people understand more. But in the current stage, this cat litter robot could help owners to clean the cat poop very well. 

### * Who has done what beforehand?

1. [Cat litter robot](https://www.litter-robot.com/)

Litter-Robot is a gravity-driven sifting automatic litter box manufactured and distributed by Whisker based in Auburn Hills, Michigan. Litter-robot was first released in 2000 and they have serval updated versions afterwards.

{{< youtube rxP58EX53Ls >}}


2. [Scoopfree self cleaning litter box](https://intl.petsafe.net/media/manuals/PAL17-14282-en-us-user-guide-with-cycle-button-v9242012.pdf)

It is a simplified cat litter robot version without a fashion control system or a beautiful shape. But it helps me most about the mechanics and evaluations for the product. The product design is straightforward and easy to handle. After doing research for this machine, I decided to use it as a prototype for my final project. Easy way to learn and have an innovation for my final project.

{{< youtube Kn368vw1jso >}}


3. [Florent Lemaire fablab documantary](https://fabacademy.org/2020/labs/agrilab/students/florent-lemaire/projects/final-project-steps/)


I found a reference about a cat litter robot, and I am lucky that someone also has similar ideas to make a cat litter robot for the fablab academy. But he used his previous machine and planned to improve it. The thing is a little different from me, but I still found many inspirations at the beginning, especially to know how the machine usually worked.

### * What will you design?

I designed a self-cleaning cat litter robot which helps the owners clean the cat poop by smartphone. But at this stage, I didn’t have a good connection between smartphones and the machine. I will continue afterwards. Right now you could make the machine run by the computer.

 I also made the box structure with a laser cutter machine, and the scoop, the litter box and the collector with a Waterjet cutter machine. My PCB boards were made by a milling machine in the fablab. I used bending machines to bend acrylic and sheet metal.

In my project, three PCB boards were made, which includes a main attiny-1614 board, a tmc2208 for driving stepper motor and a board for the power which includes 12V and 5V. 


### * What materials and components will be used? Where will they come from? How much will they cost?

**Bill of Materials**

| Name&nbsp;&nbsp;&nbsp;     | Description&nbsp;&nbsp;&nbsp;  | Links&nbsp;&nbsp;&nbsp;  | Amount     | Unit Price
| ---------- | --------- | ----------------- | ---------- |---------- |
|   Plywood  |   5mm 900x600mm Koskiply plywood  |     [  KOSKISEN](https://www.thinplywood.com/product/koskiply-birch-economy/)    |    1  |   €11.73  |
|   Acrylic  |   6mm 450x450mm Acrylic sheet |    [cutmyplastic](https://www.cutmyplastic.co.uk/acrylic-sheet/clear/6mm/L100-W50/)  |    1 |   €0.49  |
|   Acrylic  |   10mm 450x450mm Acrylic sheet |    [cutmyplastic](https://www.cutmyplastic.co.uk/acrylic-sheet/clear/10mm/L100-W50/)  |    0.5  |   €0.8 |
|   Stepper motor  |   Stepper Motor - 68 oz.in (400 steps/rev)  |     [Sparkfun](https://www.sparkfun.com/products/10846)   |    1  |   $19.50  |
|   Servo motor  |   SERVOMOTOR RC 4.8-6V HITEC HS422  |       [Digi-Key](https://www.digikey.fi/en/products/detail/dfrobot/SER0002/7067669?utm_adgroup=General&utm_source=google&utm_medium=cpc&utm_campaign=Smart%20Shopping_Product_Zombie%20SKU&utm_term=&productid=7067669&gclid=Cj0KCQjw2MWVBhCQARIsAIjbwoMChXoRMKuDwxjvCZf2HRnzJjfH0HC0m-9-2ALlhvZQB-BFjSfly6caAk-IEALw_wcB)   |    2  |  €19.82  |
|   Weight sensor  |  Load Cell - 10kg, Straight Bar (TAL220)  |        [Sparkfun](https://www.sparkfun.com/products/13329)  |    1  |   $9.50  |
|   Halleffect sensor  |   	SENSOR ANALOG -40C-125C SOT23-3  |    [Digi-Key](https://www.digikey.fi/en/products/detail/analog-devices-inc.-maxim-integrated/DS60R%2BT%26R/7652953?utm_adgroup=&utm_source=google&utm_medium=cpc&utm_campaign=Shopping_Supplier_Maxim%20Integrated_8022_Co-op&utm_term=&productid=7652953&gclid=Cj0KCQjw2MWVBhCQARIsAIjbwoOHTrJ4so8BMT8QqmSVsF7iYuCxipw2k0NO8gAQRG4P8mguMnnFCTIaApDiEALw_wcB)   |    2  |   €2.85  |
|   Magnet  |   Magnet Neodymium Iron Boron (NdFeB) N35 0.625" Dia x 0.063" H (15.9mm x 1.59mm)  |         [Digi-Key](https://www.digikey.fi/en/products/detail/radial-magnets-inc/8173/5400487)  |    3  |   €0.76 |
|   Attiny1614  |   	IC MCU 8BIT 16KB FLASH 14SOIC  |       [Digi-Key](https://www.digikey.com/en/products/detail/microchip-technology/ATTINY1614-SSNR/7354616?s=N4IgTCBcDaIIIBUEEkByBNAjANkwFgFoBlI1AJQGEEDUAREAXQF8g)  |    1  |   €0.78  |
|   UPDI pinheader  |   PinHeader_2x03_P2.54mm_Vertical_SMD |   [Digi-Key](https://www.digikey.fi/en/products/detail/metz-connect-usa-inc/PM20C03VBDN/12342899)   |    1  |   €0.43  |
|   Screw_Terminal  |   Screw_Terminal_01x02_P3.5mm |     [Sparkfun](https://www.sparkfun.com/products/8432)     |    5  |   $1.05  |
|  Pinheader   |   PinHeader_1x06_P2.54mm_Vertical_THT_D1mm  |     [Digi-Key](https://www.digikey.fi/en/products/detail/w%C3%BCrth-elektronik/62000311121/16579778)   |    4  |   €0.33  |
|   Pinheader  |   PinHeader_2x02_P2.54mm_Vertical_SMD  |        [Digi-Key](https://www.digikey.fi/en/products/detail/metz-connect-usa-inc/PM20C03VBDN/12342899)    |    2  |   €0.43  |
|   Pinheader  |    PinHeader_1x08_P2.54mm_Vertical_THT_D1.4mm  |     [Digi-Key](https://www.digikey.fi/en/products/detail/amphenol-cs-commercial-products/G800LQ302018HR/13683212)    |    2  |   €0.26  |
|   MotorDriver_StepStick_Generic  |   TMC2208SILENTSTEPSTICK  |    [Digi-Key](https://www.digikey.com/en/products/detail/trinamic-motion-control-gmbh/tmc2208silentstepstick/6873626)   |    1  |   $9.58 |
|   Regulator_Linear_LM2940IMP-5.0V-1A  |   1A 5.0V Linear Voltage Regulator IC 1 Output SOT-223 |    [Digi-Key](https://www.digikey.com/en/products/detail/onsemi/NCP1117ST50T3G/921289)    |    1  |   $0.67  |
|   Polarized capacitor |   CP_Elec_D10.0mm  |  [MOUSER](https://eu.mouser.com/c/semiconductors/discrete-semiconductors/diodes-rectifiers/schottky-diodes-rectifiers/?package%20%2F%20case=SOT-23)     |    1  |   €0,656|
|  D_Schottky_100V_1A   |   diode schottky |  [MOUSER](https://eu.mouser.com/c/semiconductors/discrete-semiconductors/diodes-rectifiers/schottky-diodes-rectifiers/?package%20%2F%20case=SOT-23)   |    1 |   €0.514  |
| HX711  |   SparkFun Load Cell Amplifier - HX711  |     [Sparkfun](https://www.sparkfun.com/products/13879)   |    1  |   $10.95  |














### * What parts and systems will be made?

1. Plywood parts-cat litter box structure.
2. Acrylic parts-structure for linear movement.
3. Metal parts- cat litter box, scoop and poop collector.
4. PCB boards.
5. Sensors- a weight sensor, two halleffect sensors.
6. Motors- One stepper motor, two servo motors.
7. Linear motion system.

 {{< postimage url = "/images/W18-componenet.jpg" alt ="throwing moon blocks" imgw ="50%">}} 

### * What processes will be used?


1. Laser cutting the plywood pieces with 5mm thickness for the cat litter robot main structure and the cover.
2. Laser cutting the acrylic board with 6mm and 10mm thickness for the main structure of machine linear movement.
3. Waterjet cutter machine cutting the metal pieces for the litter box, the scoop and the poop collector.
4. Milling PCB boards.
5. Embedded programming tests the sensors and motors.
6. Bending machine for bending the acrylic and metal pieces.
7. Assembling all the parts in the end.


### * What questions need to be answered?

Q: how did you make the communication with the weight sensor and stepper motor board?

A： The basic idea for the code is when the weight is more than 5kg which is almost the weight of one cat, the stepper motor stops. 
Actually, I didn’t use serial communication to build communication with different boards this time. I just used one package code for all the commands from the boards. Every command is transferred through my super board, Attiny-1614, which I thought was easier for me to drive the motor. But the problem is that the weight sensor needs analog reading, but the other sensors(hall effect sensors) and motors just need digital reading. So when the motor started to move, it took a long time to wait for the weight sensor reading which I didn’t think about. I am thinking of using serial communication afterwards. 


Q: What you didn't think about at first, but in the final stages became a big problem？

A: The wires! Even though at the beginning  I designed some space for the wires connecting with the boards and power, it is not enough! I should manage it very well afterwards, which probably makes my cat litter robot physically bigger. But I want to save more space for the cat litter working platform, rather than wires.


Q: Does the hall effect sensor work well?

A: To my surprise, the Hall effect sensor worked very well this time. it is easier to detect the distance. So when the linear motion bearing is very close to the hall effect, the stepper motor stops. But at the beginning, I found the numbers change in the serial monitor are not obvious, so I added more magnets. Changing is better.


Q: Which part is most important for your final project?

A: For my final project, I think the most important part is the programming test part. Because I have many parts which are needed to cooperate together. I took a long time to test the servo motor, stepper motor, weight sensor and hall effect sensors. Even though they worked very well separately, there are still many problems for communication. After testing the code, I know I should improve my PCB boards or design parts. That is the most straightforward way to evaluate my final project.

### * What worked? What didn't?

I think most parts worked very well, except my boards burned many times in between. At least now I know the board that urgently needs to be improved is the tmc2208 which drives the stepper motor. During the test period, the power supply machine shows the current raised to 1A from 0.3A immediately. Now I can't figure out why. But when I disconnect the tmc2208 with my power board, the current is very normal. So I think the problem happened on the tmc2208 board.

### * How was it evaluated?

The mechanics parts work very well which means the main structure is strong enough for linear movement and supports one cat weight. The weight sensor needs to detect the cat weight and could read the weight number. The hall effect sensors should read the number when the magnet is closer. They all need to make communication with servo motors and the stepper motor. 