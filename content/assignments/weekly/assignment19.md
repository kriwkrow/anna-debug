
+++
title = "Week 19：Invention, IP and Income"
description = "Giving credit to the images used in the demo"
summary = "This week you have to gain a basic understanding about licensing and copyright. You should update your documentation page with a copyright notice and prepare for final project presentation."
date = "2022-06-15"
author = "Anna Li"
feature_image = "/images/W19-image.jpg"
+++

# This week's assignment
* Describe what you plan to do with your final project in the future.
* Explore and compare 3 different open source licences.
* Add copyright notice to your documentation website with a licence of your choice.
* Prepare a 1 minute long video presenting your final project (1080p, 25fps, mp4, <10MB) and add it to your final project page as well as the root of your website.
* Prepare a 1920x1080 px slide representing your final project and add it to your final project page as well as the root of your website.
* Submit a link to your assignment page here.





### * A dissemination plan for my final project

For my final project, I actually haven't been ambitious to turn it into an industry product. I want to use it by myself as my original plan, but after I was all in my final project this semester, I found design for cat litter is very important especially for multi-cat families. Paying more attention to the cat’s health will help owners avoid early disease. Until now I don't have a dissemination plan yet.

### * Future possiblities

For my current final project, I wish the PCB boards could be improved and my stepper motor driving board could be stable. And I will also test the communication between the weight sensor and stepper motor, I found the communication was slow until now. I hope to build a mobile phone interface to connect the machine to the mobile phone.
I wish the cat litter robot is not only for cleaning but also to provide some healthy data for the cats in the future. 


### * The process of my final project(continuously updated)

I organized my final project process and updated the information in the previous weeks which made it easier for me and other visitors to find what I did for my final project. I add a subtitle **My final project** for each week. 

1. Cat litter robot main body making-Plywood, Acrylic and Metal cutting.

 You could find more on [Wildcard Week](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment13/)

2. PCB boards.

You could find more on [Output Devices](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment12/). 

3. Sensor testing.

You could find more on [Intput Devices](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment14/). 

4. Programming test.

You could find more on [Networking and Communication](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment15/). 

5. Interface.

You could find more on [Interface and Application Programming](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/assignments/weekly/assignment16/). 



 {{< postimage url = "/images/W19-CAT LITTER POSTER.jpg" alt ="throwing moon blocks" imgw ="100%">}} 


{{< youtube nof1048ENHs >}}















 













